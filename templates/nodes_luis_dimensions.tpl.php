<?php

$output = '<div class="product-dimensions">';
$output .= t('Dimensions (length x width x height)') . ': ' .
    $variables['length'] . ' x ' . $variables['width'] . ' x ' .
    $variables['height'] . ' cm.';
$output .= '</div>';

print $output;

